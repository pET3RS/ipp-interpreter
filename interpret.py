import argparse
import os
import sys
import xml.etree.ElementTree as ET
from enum import Enum
import re


class ArgTypeGroups(Enum):
    """
    Enum representing all the groups of types for IPPcode21
    """
    VAR = 1
    SYMB = 2
    LABEL = 3
    TYPE = 4


class ProgramArguments:
    """
    Class handling the arguments of this program. Checks the validity of the arguments.
    """


    source_path = sys.stdin
    input_path = sys.stdin

    def __init__(self):
        """
        Parses the arguments and checks their validity. If any error occurs the program is
        terminated with proper exit code.
        """
        arguments = self.parse_args()

        # Store the pats form arguments
        if arguments.get("source") is not None:
            file = arguments.get("source")
            if not os.access(file, os.F_OK) or not os.access(file, os.R_OK):
                self.exit_with_error_message(11, "Couldn't access the specified source file!")
            self.source_path = file
        if arguments.get("input") is not None:
            file = arguments.get("input")
            if not os.access(file, os.F_OK) or not os.access(file, os.R_OK):
                self.exit_with_error_message(11, "Couldn't access the specified input file!")
            self.input_path = file

        # Check if at least one of the files is specified
        if self.input_path == sys.stdin and self.source_path == sys.stdin:
            self.exit_with_error_message(10, "At least one of the arguments (source, input) must be specified! "
                                             "Use --help for more info.")

    def parse_args(self):
        """
        Uses argparse to parse arguments and handles occurrence of a argument error by terminating the program
        with proper exit code.

        :return: a dictonary containing the arguments
        """

        parser = argparse.ArgumentParser(prog="python3 interpret.py", description="Interprets IPPcode21 in XML form.",
                                         epilog="The source and input parameters are optional, however one of "
                                                "them must be present!")
        parser.add_argument("--source", metavar="FILE",
                            help="path to XML source file one wants to interpret")
        parser.add_argument("--input", metavar="FILE",
                            help="specifies the file from which should program read its imput [default: STDIN]")
        try:
            args=vars(parser.parse_args())
        except argparse.ArgumentError as err:
            self.exit_with_error_message(10, f"{err}")

        return args

    @staticmethod
    def exit_with_error_message(exit_val, msg):
        """
        This method provides unification of the error message for convenience.
        It terminates the program with specified exit value and prints error
        message to the stderr

        :param exit_val:  the exit value of the program
        :param msg:  the message printed as error
        """
        print(f"[E]: {msg}", file=sys.stderr)
        sys.exit(exit_val)


class InstructionTable:
    """
    This class holds all the information about instructions from IPPcode21.
    """

    instructions_table = [
        {"opcode": "MOVE", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB},
        {"opcode": "CREATEFRAME", "argc": 0},
        {"opcode": "PUSHFRAME", "argc": 0},
        {"opcode": "POPFRAME", "argc": 0},
        {"opcode": "DEFVAR", "argc": 1, "arg1": ArgTypeGroups.VAR},
        {"opcode": "CALL", "argc": 1, "arg1": ArgTypeGroups.LABEL},
        {"opcode": "RETURN", "argc": 0},
        {"opcode": "PUSHS", "argc": 1, "arg1": ArgTypeGroups.SYMB},
        {"opcode": "POPS", "argc": 1, "arg1": ArgTypeGroups.VAR},
        {"opcode": "ADD", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "SUB", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "MUL", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "IDIV", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "LT", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "GT", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "EQ", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "AND", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "OR", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "NOT", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB},
        {"opcode": "INT2CHAR", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB},
        {"opcode": "STRI2INT", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "READ", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.TYPE},
        {"opcode": "WRITE", "argc": 1, "arg1": ArgTypeGroups.SYMB},
        {"opcode": "CONCAT", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "STRLEN", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB},
        {"opcode": "GETCHAR", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "SETCHAR", "argc": 3, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "TYPE", "argc": 2, "arg1": ArgTypeGroups.VAR, "arg2": ArgTypeGroups.SYMB},
        {"opcode": "LABEL", "argc": 1, "arg1": ArgTypeGroups.LABEL},
        {"opcode": "JUMP", "argc": 1, "arg1": ArgTypeGroups.LABEL},
        {"opcode": "JUMPIFEQ", "argc": 3, "arg1": ArgTypeGroups.LABEL, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "JUMPIFNEQ", "argc": 3, "arg1": ArgTypeGroups.LABEL, "arg2": ArgTypeGroups.SYMB, "arg3": ArgTypeGroups.SYMB},
        {"opcode": "EXIT", "argc": 1, "arg1": ArgTypeGroups.SYMB},
        {"opcode": "DPRINT", "argc": 1, "arg1": ArgTypeGroups.SYMB},
        {"opcode": "BREAK", "argc": 0}]

    def get_instruction_format(self, opcode):
        """
        Returns the information about the instruction form the instruction table.

        :param opcode: The opcode of instruction one wants to get information about
        :return: None if the opcode doesn't exist or a the proper instruction information form the instruction table
        """
        for instruction in self.instructions_table:
            if instruction["opcode"] == opcode.upper():
                return instruction

        # Invalid opcode!
        return None

    def is_opcode_valid(self, opcode):
        """
        Checks if the opcode is valid.

        :param opcode: the opcode one needs to verify
        :return:  True if the opcode is valid or False if it is not valid
        """
        for instruction in self.instructions_table:
            if instruction["opcode"] == opcode.upper():
                return True
        return False

    def get_expected_arg_count(self, opcode):
        """
        Retrieves the expected argument count from instruction table by specified opcode.

        :param opcode:  the opcode of the instruction from which one wants expected argument count
        :return: None if the opcode doesn't exist or the expected argument count
        """

        for instruction in self.instructions_table:
            if instruction["opcode"] == opcode.upper():
                return instruction["argc"]

        # Invalid opcode!
        return None


class SourceParser:
    """
    Class which handles parsing of the XML IPPcode21 representation
    and creates a internal representation of the IPPcode21
    """

    def __init__(self, source_file, instr_table):
        """
        Main body of whole source parsing. The parsing is done in 2 passes
        first orders the instructions correctly and the second parses them
        into an internal representation of IPPcode2. The method terminates
        the program on any error occurrence with proper exit value.

        :param source_file:  string containing the path to source file - IPPcode21 representated in XML
        :param instr_table:  the InstructionTable instance
        """

        self.source_file = source_file
        self.program = list()

        try:
            self.XMLtree = ET.parse(self.source_file)
        except ET.ParseError as err:
            self.exit_with_error_message(31, f"XML file parsing failed! {err}")

        self.instruction_table = instr_table

        root = self.XMLtree.getroot()
        self.validate_root_element(root)

        # First pass of xml file to determine order of instructions
        order_list = list()
        for instr in root:
            if "order" not in instr.attrib.keys():
                msg = "There must be an attribute order in instruction element!"
                print(f"[E]: {msg}", file=sys.stderr)
                sys.exit(32)

            if not instr.attrib["order"].isdigit():
                msg = "Attribute order in instruction element should be int!"
                print(f"[E]: {msg}", file=sys.stderr)
                sys.exit(32)

            if instr.attrib["order"] in order_list:
                msg = "Attribute order in instruction element should be unique!"
                print(f"[E]: {msg}", file=sys.stderr)
                sys.exit(32)

            if int(instr.attrib["order"]) <= 0:
                msg = "Attribute order in instruction element should be positive integer!"
                print(f"[E]: {msg}", file=sys.stderr)
                sys.exit(32)

            order_list.append(instr.attrib["order"])

        sorted_root = sorted(root, key=lambda child: int(child.get("order")))

        # Second pass to convert XML to internal representation
        for instruction in sorted_root:
            if instruction.tag != "instruction":
                self.exit_with_error_message(32, f"Unexpected {instruction.tag} element inside program element!")
            self.parse_instruction(instruction)

    def parse_instruction(self, instruction_element):
        """
        Parses the instruction element form xml representation and creates internal representation
        of this instruction which is then appended to a list of instruction which together form the
        representation of the IPPcode21 user of this class wants to parse. The method terminates the
        program on any error occurrence with proper exit value.

        :param instruction_element: the XML element representation of the instruction
        """
        instruction = list()

        expected_arg_count = self.validate_instruction_element(instruction_element)

        opcode = instruction_element.attrib["opcode"]
        instruction.append(opcode.upper())

        arguments = list()
        arg_count = 0

        sorted_instruction_element_arguments = sorted(instruction_element, key=lambda child: child.tag)
        for argument in sorted_instruction_element_arguments:
            arg_count += 1
            self.validate_arg_element(opcode, argument, arg_count)

            arg_type = argument.attrib["type"]
            arg_value = argument.text

            if arg_value is None:
                arg_value = ""

            arguments.append({"type": arg_type, "value": arg_value})

        instruction.append(arguments)

        if expected_arg_count != arg_count:
            self.exit_with_error_message(32,
                                         f"Unexpected number of arguments in an instruction! Found {arg_count},"
                                         f" but expected {expected_arg_count}.")

        self.program.append(instruction)

    def validate_root_element(self, root):
        """
        Checks the root element of the XML representation for the structural errors. The method
        terminates the program on any error occurrence with proper exit value.

        :param root: the root element of the XML representation of the IPPcode21 program
        """

        if root.tag != "program":
            self.exit_with_error_message(32, "Wrong name of the root XML element")

        attributes = list(root.attrib.keys())

        for attribute in attributes:
            if attribute not in ["language", "name", "description"]:
                self.exit_with_error_message(32, "Wrong attribute in program element!")

        if "language" not in attributes:
            self.exit_with_error_message(32, "The program element does not contain language attribute!")

        if root.attrib["language"] != "IPPcode21":
            self.exit_with_error_message(32, "The attribute 'language' of the 'program' element should hold "
                                             "only value 'IPPcode21'!")

    def validate_instruction_element(self, instruction):
        """
        Checks a instruction element for any error and returns the expected
        number of arguments. The method terminates the program on any error
        occurrence with proper exit value.

        :param instruction:  the XML element representing a instruction
        :return: expected argument count for the instruction
        """

        instruction_attributes = instruction.attrib

        attribute_names = list(instruction_attributes.keys())

        if "order" not in attribute_names or "opcode" not in attribute_names:
            self.exit_with_error_message(32, "Unexpected attributes in an instruction element!")

        opcode = instruction_attributes["opcode"]
        argc = self.instruction_table.get_expected_arg_count(opcode)

        if argc is None:
            self.exit_with_error_message(32, "Invalid opcode!")

        return argc

    def validate_arg_element(self, opcode, arg, order):
        """
        Checks a XML representation of IPPcode21 argument for any errors. Semantic and
        lexical checks are made to ensure the validity of the code it self.
        The method terminates the program on any error occurrence with proper exit value.

        :param opcode: opcode of the instruction this argument belongs to
        :param arg: the XML element representation the argument
        :param order: the expected order of the attribute
        """

        variable_pattern = re.compile(r"^(GF|LF|TF)@[a-zA-Z_\-$%&*?!][0-9a-zA-Z_\-$%&*?!]*$")
        label_pattern = re.compile(r"^[a-zA-Z_\-$%&*?!][0-9a-zA-Z_\-$%&*?!]*$")
        type_pattern = re.compile(r"^(int|bool|string)$")
        bool_pattern = re.compile(r"^(true|false)$")
        arguments_pattern = re.compile(r"^arg[123]$")
        symb_type_patern = re.compile(r"^(int|string|bool|nil|var)$")

        # Argument tag checks
        if re.match(arguments_pattern, arg.tag) is None:
            self.exit_with_error_message(32, "Unexpected argument element name!")
        if order != int(arg.tag[-1]):
            self.exit_with_error_message(32, "Wrong argument order!")

        # Get expected argument type group
        instruction = self.instruction_table.get_instruction_format(opcode)

        if arg.tag not in instruction.keys():
            self.exit_with_error_message(32, f"Unexpected argument {arg.tag}!")
        expected_argument_type = instruction[arg.tag]

        # Check if expected argument type group matches the argument form xml
        if expected_argument_type == ArgTypeGroups.SYMB:

            # First, check if the type attribute matches any of the types form SYMB group.
            if re.match(symb_type_patern, arg.attrib["type"]) is None:
                self.exit_with_error_message(32, "Unexpected argument type!")

            # Now, when the type is correct, eliminate the wrong values

            if arg.attrib["type"] == "int":

                integer = arg.text
                if arg.text[0] == "-":
                    integer = arg.text[1:]

                if not integer.isdigit():
                    self.exit_with_error_message(32, "Integer argument doesn't match integer format")

            elif arg.attrib["type"] == "string":
                self.validate_string(arg.text)
            elif arg.attrib["type"] == "bool" and re.match(bool_pattern, arg.text) is None:
                self.exit_with_error_message(32, "Bool argument doesn't match bool format")
            elif arg.attrib["type"] == "nil" and arg.text != "nil":
                self.exit_with_error_message(32, "Nil argument doesn't match nil format")
            elif arg.attrib["type"] == "var" and re.match(variable_pattern, arg.text) is None:
                self.exit_with_error_message(32, "The argument doesn't match the variable format!")

        elif expected_argument_type == ArgTypeGroups.VAR:
            if arg.attrib["type"] != "var":
                self.exit_with_error_message(32, "Unexpected argument type!")
            if re.match(variable_pattern, arg.text) is None:
                self.exit_with_error_message(32, "Argument doesn't match the variable format!")
        elif expected_argument_type == ArgTypeGroups.LABEL:
            if arg.attrib["type"] != "label":
                self.exit_with_error_message(32, "Unexpected argument type!")
            if re.match(label_pattern, arg.text) is None:
                self.exit_with_error_message(32, "Argument doesn't match the label format!")
        elif expected_argument_type == ArgTypeGroups.TYPE:
            if arg.attrib["type"] != "type":
                self.exit_with_error_message(32, "Unexpected argument type!")
            if re.match(type_pattern, arg.text) is None:
                self.exit_with_error_message(32, "Argument doesn't match the type format!")

    def validate_string(self, string):
        """
        Checks the string for anything that violates the rules set for string by the IPPcode21.

        :param string: the string in IPPcode21 format
        :return:  True if valid False if not valid
        """

        # An empty string is represented by None in the xml representation.
        if string is None:
            return True

        str_len = len(string)

        for idx in range(str_len):
            if string[idx] == "#":
                self.exit_with_error_message(32, "String shouldn't contain # character!")
            elif string[idx] == "\\":
                if idx + 3 >= str_len:
                    self.exit_with_error_message(32, "String contains wrong escape sequence!")
                else:
                    for i in range(1, 4):
                        if not string[idx + i].isdigit():
                            self.exit_with_error_message(32, "String contains wrong escape sequence!")
        return True

    def exit_with_error_message(self, exit_val, msg):
        """
        This method unifies the error message and adds valuable information of the instruction index where
        the error occured. Exits the program with proper value and prints the specified error message.

        :param exit_val: exit value one wants the program to exit with
        :param msg: the error message
        :return:
        """
        print(f"[E]: {msg} Instruction index: {len(self.program)}", file=sys.stderr)
        sys.exit(exit_val)

    def dump_program(self):
        """
        Prints the internal representation of the IPPcode21 created form xml representation.
        """
        for instruction in self.program:
            print(instruction)


class Interpreter:

    def __init__(self, input_file):
        """
        Initializes the memory frames and variables for storing internal notes about the
        interpreted program. If needed reads the input file and stores it too for later use.

        :param input_file: the file one wants to read input form
        """

        self.input_file = input_file

        # Read the input file if it isn't STDIN
        if self.input_file != sys.stdin:
            self.input_file_contents = []
            self.current_input_file_contents_index = 0
            with open(self.input_file) as f:
                self.input_file_contents = f.readlines()

        self.current_instruction_index = 0
        self.program = list()
        self.labels = dict()        # { "label_name": label_index + 1 }
        self.calls_stack = list()   # list of positions to return to (already incremented)
        self.data_stack = list()    # [[type, value], ...]

        # Frames initialization ( a frame - {"var_name": [type, value], ...} )
        self.global_frame = dict()
        self.local_frame_stack = list()  # [lframe1, lfram2, ..., lframeHEAD]
        self.temporary_frame = None

    def get_frame(self, type):
        """
        Retrieves the frame by its type. If the frame doesn't exist returns None

        :param type: the type of frame one wants to get GF|LF|TF
        :return: None if not found or dictionary containing the frame
        """

        if type == "GF":
            return self.global_frame

        if type == "LF":

            if len(self.local_frame_stack) == 0:
                return None
            else:
                return self.local_frame_stack[-1]

        if type == "TF":
            return self.temporary_frame

    def interpret_program(self, program):
        """
        This method starts the interpretation of the program. Implements two passes - First for
        taking notes about defined labels and their position and the second for the interpretaiton
        itself.

        :param program: the internal representation of the program
        """

        self.program = program

        # First pass to note labels positions
        for instr in self.program:
            if instr[0] == "LABEL":
                args = instr[1]
                if args[0]["value"] in self.labels.keys():
                    msg = "Label redefinition!"
                    print(f"[E]: {msg} Instruction number: {self.program.index(instr)+1}", file=sys.stderr)
                    sys.exit(52)
                # No increment because the main loop increments every time after instruction
                self.labels[args[0]["value"]] = self.program.index(instr)

        # Second pass to interpret the program
        while self.current_instruction_index < len(self.program):
            instruction = self.program[self.current_instruction_index]

            opcode = instruction[0]

            # Skip labels (handled in the first pass)
            if opcode == "LABEL":
                self.current_instruction_index += 1
                continue

            arguments = instruction[1]    # [{"type": var, "value": GF@a}, ... ]

            # interpret the instruction
            self.function_table[opcode](self, arguments)

            self.current_instruction_index += 1

    def exit_with_error_msg(self, exit_val, msg):
        """
        Unifies the error message and adds valuable information to it in form of an instruction number
        on which the  error occurred.
        :param exit_val: the value one wants to exit the program with
        :param msg: the error message
        """
        print(f"[E]: {msg} Instruction number: {self.current_instruction_index+1}", file=sys.stderr)
        sys.exit(exit_val)

    def dump_frames(self):
        """
        Prints contents of the frames to the stderr
        :return:
        """
        print("GF:", file=sys.stderr)
        for var in self.global_frame:
            print("\t", end="", file=sys.stderr)
            print(var, file=sys.stderr)
        print("LF stacks:", file=sys.stderr)
        if self.get_frame("LF") is None:
            print("\tempty", file=sys.stderr)
        else:
            for lf in self.local_frame_stack:
                print("LF:", file=sys.stderr)
                if len(lf) == 0:
                    print("\tempty", file=sys.stderr)
                for var in lf:
                    print("\t", file=sys.stderr)
                    print(var, file=sys.stderr)
        print("TF stack",file=sys.stderr)
        if self.temporary_frame is None:
            print("\tempty", file=sys.stderr)
        else:
            for var in self.temporary_frame:
                print(var, file=sys.stderr)

    def get_var_frame(self, variable):
        """
        Retrieves the proper variable frame.

        :param variable: the variable of which one wants to get the frame
        :return: the frame of variable
        """

        #  variable format {"type": "var", "value": GF@a}
        var_frame = self.get_frame(variable["value"][:2])
        if var_frame is None:
            self.exit_with_error_msg(55, f"Frame {variable['value'][:2]} doesn't exist!")
        if variable["value"][3:] not in var_frame.keys():
            self.exit_with_error_msg(54, f"Variable {variable['value']} doesn't exist!")
        return var_frame

    def get_value_of_symb_by_type(self, symb, type, operand_position):
        """
        Retrives the value of the symb group of types in IPPcode21 by specified type.

        :param symb: the internal representation of the symb group type value
        :param type: the expected type
        :param operand_position: the position of the operand
        :return:
        """
        if symb["type"] == "var":
            symb_frame = self.get_var_frame(symb)
            symb_var_name = symb["value"][3:]
            if len(symb_frame[symb_var_name]) != 2:
                self.exit_with_error_msg(56, f"The {operand_position} operand is uninitialized variable!")
            if symb_frame[symb_var_name][0] != type:
                self.exit_with_error_msg(53, f"The {operand_position} operand should be type {type}!")
            symb_val = symb_frame[symb_var_name][1]
        else:
            if symb["type"] != type:
                self.exit_with_error_msg(53, f"The {operand_position} operand should be type {type}!")
            symb_val = symb["value"]

        if type == "int":
            symb_val = int(symb_val)

        return symb_val

    def get_type_of_symb_value(self, symb):
        """
        Retirieves the type of concrete symb group of types
        :param symb: the internal representation of symb group type value
        :return: the type of the value
        """
        if symb["type"] == "var":
            symb_frame = self.get_var_frame(symb)
            symb_var_name = symb["value"][3:]
            if len(symb_frame[symb_var_name]) != 2:
                self.exit_with_error_msg(56, f"The {symb['value']} is uninitialized!")
            return symb_frame[symb_var_name][0]
        else:
            return symb["type"]

    @staticmethod
    def convert_to_python_bool(ippcode_bool):
        """
        Converts IPPcode21 bool to python bool
        :param ippcode_bool: the bool in IPPcode21 representation
        :return: python representation of the bool
        """
        if ippcode_bool == "true":
            return True
        else:
            return False

    @staticmethod
    def convert_to_ippcode_bool(boolean):
        """
        Converts python bool to IPPcode21 bool
        :param boolean: python boolean
        :return: IPPcode21 boolean
        """
        if boolean:
            return "true"
        else:
            return "false"

    @staticmethod
    def convert_escapes_in_string(string):
        """
        Simple converor from IPPcode21 string representation to python representation.
        :param string: the IPPcode21 string
        :return: python string
        """
        string_without_escapes = ""
        i = 0
        while i < len(string):
            character = string[i]
            if character == "\\":
                escape_ord = int(string[i + 1:i + 4])
                string_without_escapes += chr(escape_ord)
                i += 4
                continue
            else:
                string_without_escapes += character
                i += 1
        return string_without_escapes

    def convert_string_to_ippcode_style(self, string):
        """
        Simple converter of python string to IPPcode21 string
        :param string: the python string
        :return: IPPcode21 string
        """
        new_string = ""
        for character in string:
            char_ord = ord(character)
            if char_ord in range(0, 33) or char_ord == 35 or char_ord == 92:
                new_string += f"\\0{char_ord}"
            else:
                if character == "#":
                    self.exit_with_error_msg(32, "String shouldn't contain # character!")
                new_string += character
        return new_string

    # --- interpretation functions ---
    def interpret_move(self, args):
        dest = args[0]
        origin = args[1]

        dest_frame = self.get_var_frame(dest)

        if origin["type"] == "var":
            origin_frame = self.get_var_frame(origin)
            if len(origin_frame[origin["value"][3:]]) != 2:
                self.exit_with_error_msg(56, "The second operand is uninitialized variable!")
            dest_frame[dest["value"][3:]] = origin_frame[origin["value"][3:]]
            return

        dest_frame[dest["value"][3:]] = [origin["type"], origin["value"]]

    def interpret_createframe(self, args):
        self.temporary_frame = dict()

    def interpret_pushframe(self, args):

        if self.temporary_frame is None:
            self.exit_with_error_msg(55, "Temporary frame is undefined!")

        self.local_frame_stack.append(self.temporary_frame)
        self.temporary_frame = None

    def interpret_popframe(self, args):

        current_lf = self.get_frame("LF")

        if current_lf is None:
            self.exit_with_error_msg(55, "There is no local frame to pop!")

        self.temporary_frame = self.local_frame_stack.pop()

    def interpret_defvar(self, args):
        new_var = args[0]
        frame = self.get_frame(new_var["value"][:2])

        # Check if frame exists
        if frame is None:
            self.exit_with_error_msg(55, f"Frame {new_var['value'][:2]} doesn't exist!")

        # Check if is already defined
        new_var_name = new_var["value"][3:]
        if new_var_name in frame.keys():
            self.exit_with_error_msg(52, f"Variable {new_var['value']} already defined!")

        # Define variable (uninitialized)
        frame[new_var_name] = []

    def interpret_call(self, args):

        # Store return point
        # incremented because the specification defines it that way
        self.calls_stack.append(self.current_instruction_index+1)

        # Check if label exists
        label = args[0]["value"]
        if label not in self.labels.keys():
            self.exit_with_error_msg(52, f"Label {label} not defined!")

        # Jump to next instruction (the index is already incremented)
        self.current_instruction_index = self.labels[label]

    def interpret_return(self, args):
        # Check if there is index in calls stack
        if len(self.calls_stack) == 0:
            self.exit_with_error_msg(56, "There is nowhere to return!")

        # Jump to next instruction
        # decremented to the call position because the call specifies incremented value
        # but the main loop increments by it self each time. (purely adaptation to possible testing)
        self.current_instruction_index = self.calls_stack.pop()-1

    def interpret_pushs(self, args):
        arg = args[0]

        if arg["type"] == "var":
            frame = self.get_var_frame(arg)
            value_to_push = frame[arg["value"][3:]]
            if len(value_to_push) != 2:
                self.exit_with_error_msg(56, "The argument is uninitialized variable!")
        else:
            value_to_push = [arg["type"], arg["value"]]

        self.data_stack.append(value_to_push)

    def interpret_pops(self, args):

        # Check if there is a value to pop
        if len(self.data_stack) == 0:
            self.exit_with_error_msg(56, "There is nothing to pop!")

        dest = args[0]
        dest_frame = self.get_var_frame(dest)
        dest_frame[dest["value"][3:]] = self.data_stack.pop()

    def interpret_add(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")
        new_val = symb1_val + symb2_val

        dest_frame[dest["value"][3:]] = ["int", new_val]

    def interpret_sub(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")
        new_val = symb1_val - symb2_val

        dest_frame[dest["value"][3:]] = ["int", new_val]

    def interpret_mul(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")
        new_val = symb1_val * symb2_val

        dest_frame[dest["value"][3:]] = ["int", new_val]

    def interpret_idiv(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")

        if symb2_val == 0:
            self.exit_with_error_msg(57, "Division by zero!")

        new_val = symb1_val // symb2_val
        dest_frame[dest["value"][3:]] = ["int", new_val]

    def interpret_lt(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != symb2_type:
            self.exit_with_error_msg(53, "The compared values must be the same type!")

        if symb1_type == "nil":
            self.exit_with_error_msg(53, "Can't compare nil type using LT!")

        symb1_val = self.get_value_of_symb_by_type(symb1, symb1_type, "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, symb2_type, "third")

        if symb1_type == "bool":
            if symb1_val == "false" and symb2_val == "true":
                new_val = "true"
            else:
                new_val = "false"
        else:
            if symb1_type == "string":
                symb1_val = self.convert_escapes_in_string(symb1_val)
                symb2_val = self.convert_escapes_in_string(symb2_val)

            new_val = symb1_val < symb2_val
            if new_val == True:
                new_val = "true"
            else:
                new_val = "false"

        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_gt(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != symb2_type:
            self.exit_with_error_msg(53, "The compared values must be the same type!")

        if symb1_type == "nil":
            self.exit_with_error_msg(53, "Can't compare nil type using GT!")

        symb1_val = self.get_value_of_symb_by_type(symb1, symb1_type, "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, symb2_type, "third")

        if symb1_type == "bool":
            if symb1_val == "true" and symb2_val == "false":
                new_val = "true"
            else:
                new_val = "false"
        else:

            if symb1_type == "string":
                symb1_val = self.convert_escapes_in_string(symb1_val)
                symb2_val = self.convert_escapes_in_string(symb2_val)

            new_val = symb1_val > symb2_val
            if new_val == True:
                new_val = "true"
            else:
                new_val = "false"

        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_eq(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type == "nil" or symb2_type == "nil":
            new_val = "false"
            if symb1_type == symb2_type:
                new_val = "true"
        else:
            if symb1_type != symb2_type:
                self.exit_with_error_msg(53, "The compared values must be the same type!")

            symb1_val = self.get_value_of_symb_by_type(symb1, symb1_type, "second")
            symb2_val = self.get_value_of_symb_by_type(symb2, symb2_type, "third")

            if symb1_type == "string":
                symb1_val = self.convert_escapes_in_string(symb1_val)
                symb2_val = self.convert_escapes_in_string(symb2_val)

            new_val = "false"
            if symb1_val == symb2_val:
                new_val = "true"

        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_and(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != "bool" or symb2_type != "bool":
            self.exit_with_error_msg(53, "Both arguments should be type bool!")

        symb1_val = self.get_value_of_symb_by_type(symb1, "bool", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "bool", "third")

        symb1_val = self.convert_to_python_bool(symb1_val)
        symb2_val = self.convert_to_python_bool(symb2_val)

        new_val = self.convert_to_ippcode_bool(symb1_val and symb2_val)

        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_or(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != "bool" or symb2_type != "bool":
            self.exit_with_error_msg(53, "Both arguments should be type bool!")

        symb1_val = self.get_value_of_symb_by_type(symb1, "bool", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "bool", "third")

        symb1_val = self.convert_to_python_bool(symb1_val)
        symb2_val = self.convert_to_python_bool(symb2_val)

        new_val = self.convert_to_ippcode_bool(symb1_val or symb2_val)
        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_not(self, args):
        dest = args[0]
        symb1 = args[1]

        dest_frame = self.get_var_frame(dest)
        symb1_type = self.get_type_of_symb_value(symb1)

        if symb1_type != "bool":
            self.exit_with_error_msg(53, "The type of second argument should be bool!")

        symb1_val = self.get_value_of_symb_by_type(symb1, "bool", "second")
        symb1_val = self.convert_to_python_bool(symb1_val)

        new_val = self.convert_to_ippcode_bool(not symb1_val)

        dest_frame[dest["value"][3:]] = ["bool", new_val]

    def interpret_int2char(self, args):
        dest = args[0]
        symb1 = args[1]

        dest_frame = self.get_var_frame(dest)
        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")

        try:
            new_val = chr(symb1_val)
        except ValueError as err:
            self.exit_with_error_msg(58, f"Invalid ordinal value! {err}.")

        dest_frame[dest["value"][3:]] = ["string", new_val]

    def interpret_stri2int(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "string", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")

        string_without_escapes = self.convert_escapes_in_string(symb1_val)

        if symb2_val >= len(string_without_escapes) or symb2_val < 0:
            self.exit_with_error_msg(58, "The specified index is out of range!")

        dest_frame[dest["value"][3:]] = ["int", ord(string_without_escapes[symb2_val])]

    def interpret_read(self, args):
        dest = args[0]
        type = args[1]

        dest_frame = self.get_var_frame(dest)

        if self.input_file == sys.stdin:
            try:
                user_input = input()
            except EOFError:
                dest_frame[dest["value"][3:]] = ["nil", "nil"]
                return
        else:
            if len(self.input_file_contents) <= self.current_input_file_contents_index:
                dest_frame[dest["value"][3:]] = ["nil", "nil"]
                return
            user_input = self.input_file_contents[self.current_input_file_contents_index].strip("\n")
            self.current_input_file_contents_index += 1

        if type["value"] == "int":

            try:
                new_val = ["int", int(user_input)]
            except ValueError:
                dest_frame[dest["value"][3:]] = ["nil", "nil"]
                return

        elif type["value"] == "bool":
            new_val = ["bool", "false"]
            if user_input.lower() == "true":
                new_val = ["bool", "true"]
        else:
            # The representatoion of the string is on imput in human or IPPcode21 form?
            user_input = self.convert_string_to_ippcode_style(user_input)
            new_val = ["string", user_input]

        dest_frame[dest["value"][3:]] = new_val

    def interpret_write(self, args):
        symb = args[0]

        symb_type = self.get_type_of_symb_value(symb)
        symb_val = self.get_value_of_symb_by_type(symb, symb_type, "first")

        output = symb_val
        if symb_type == "nil":
            output = ""
        if symb_type == "string":
            output = self.convert_escapes_in_string(symb_val)

        print(output, end="")

    def interpret_concat(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "string", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "string", "third")

        dest_frame[dest["value"][3:]] = ["string", symb1_val+symb2_val]

    def interpret_strlen(self, args):
        dest = args[0]
        symb = args[1]

        dest_frame = self.get_var_frame(dest)
        symb_val = self.get_value_of_symb_by_type(symb, "string", "second")

        symb_val_without_escapes = self.convert_escapes_in_string(symb_val)

        dest_frame[dest["value"][3:]] = ["int", len(symb_val_without_escapes)]

    def interpret_getchar(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        symb1_val = self.get_value_of_symb_by_type(symb1, "string", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "int", "third")
        string_without_escapes = self.convert_escapes_in_string(symb1_val)
        if symb2_val >= len(string_without_escapes) or symb2_val < 0:
            self.exit_with_error_msg(58, "The specified index is out of range!")

        character = string_without_escapes[symb2_val]
        character = self.convert_string_to_ippcode_style(character)

        dest_frame[dest["value"][3:]] = ["string", character]

    def interpret_setchar(self, args):
        dest = args[0]
        symb1 = args[1]
        symb2 = args[2]

        dest_frame = self.get_var_frame(dest)

        dest_var_name = dest["value"][3:]

        if dest_var_name not in dest_frame.keys():
            self.exit_with_error_msg(54, "Frist argument doesn't exist!")
        if len(dest_frame[dest_var_name]) != 2:
            self.exit_with_error_msg(56, "First argument is uninitialized variable!")

        dest_type = dest_frame[dest_var_name][0]
        dest_val = dest_frame[dest_var_name][1]

        if dest_type != "string":
            self.exit_with_error_msg(53, "The destination variable isn't string!")

        dest_val_without_escapes = self.convert_escapes_in_string(dest_val)

        symb1_val = self.get_value_of_symb_by_type(symb1, "int", "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, "string", "third")

        if symb1_val >= len(dest_val_without_escapes) or symb1_val < 0:
            self.exit_with_error_msg(58, "Index out of range!")

        symb2_val_without_escapes = self.convert_escapes_in_string(symb2_val)
        if len(symb2_val_without_escapes) > 1:
            symb2_val_without_escapes = symb2_val_without_escapes[0]
        if len(symb2_val_without_escapes) == 0:
            self.exit_with_error_msg(58, "Third argument is a empty string!")

        new_string = dest_val_without_escapes[:symb1_val] + symb2_val_without_escapes + dest_val_without_escapes[symb1_val+1:]
        new_string = self.convert_string_to_ippcode_style(new_string)

        dest_frame[dest_var_name] = ["string", new_string]

    def interpret_type(self, args):
        dest = args[0]
        symb = args[1]

        dest_frame = self.get_var_frame(dest)

        symb_type = symb["type"]
        if symb["type"] == "var":
            symb_frame = self.get_var_frame(symb)
            symb_var_name = symb["value"][3:]
            if len(symb_frame[symb_var_name]) != 2:
                symb_type = ""
            else:
                symb_type = symb_frame[symb_var_name][0]

        dest_frame[dest["value"][3:]] = ["string", symb_type]

    def interpret_jump(self, args):
        label = args[0]

        if label["value"] not in self.labels.keys():
            self.exit_with_error_msg(52, f"Label {label} not defined!")

        self.current_instruction_index = self.labels[label["value"]]

    def interpret_jumpifeq(self, args):
        label = args[0]
        symb1 = args[1]
        symb2 = args[2]

        if label["value"] not in self.labels.keys():
            self.exit_with_error_msg(52, f"Label {label} not defined!")

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != symb2_type and symb1_type != "nil" and symb2_type != "nil":
            self.exit_with_error_msg(53, f"Types not matching and neither is nul ({symb1_type}, {symb2_type})!")

        symb1_val = self.get_value_of_symb_by_type(symb1, symb1_type, "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, symb2_type, "third")

        if symb1_type == "string":
            symb1_val = self.convert_escapes_in_string(symb1_val)
            symb2_val = self.convert_escapes_in_string(symb2_val)

        if symb1_val == symb2_val:
            self.current_instruction_index = self.labels[label["value"]]

    def interpret_jumpifneq(self, args):
        label = args[0]
        symb1 = args[1]
        symb2 = args[2]

        if label["value"] not in self.labels.keys():
            self.exit_with_error_msg(52, f"Label {label} not defined!")

        symb1_type = self.get_type_of_symb_value(symb1)
        symb2_type = self.get_type_of_symb_value(symb2)

        if symb1_type != symb2_type and symb1_type != "nil" and symb2_type != "nil":
            self.exit_with_error_msg(53, f"Types not matching and neither is nil ({symb1_type}, {symb2_type})!")

        symb1_val = self.get_value_of_symb_by_type(symb1, symb1_type, "second")
        symb2_val = self.get_value_of_symb_by_type(symb2, symb2_type, "third")

        if symb1_type == "string":
            symb1_val = self.convert_escapes_in_string(symb1_val)
            symb2_val = self.convert_escapes_in_string(symb2_val)

        if symb1_val != symb2_val:
            self.current_instruction_index = self.labels[label["value"]]

    def interpret_exit(self, args):
        symb = args[0]

        symb_val = self.get_value_of_symb_by_type(symb, "int", "first")

        if symb_val < 0 or symb_val > 49:
            self.exit_with_error_msg(57, "Exit value should be in [0,49] interval!")

        sys.exit(symb_val)

    def interpret_dprint(self, args):
        symb = args[0]

        symb_type = self.get_type_of_symb_value(symb)
        symb_val = self.get_value_of_symb_by_type(symb, symb_type, "first")

        print(symb_val, file=sys.stderr)

    def interpret_break(self, args):
        print(f"Current instruction INDEX: {self.current_instruction_index}", file=sys.stderr)
        print(f"Current instruction: {self.program[self.current_instruction_index]}", file=sys.stderr)
        print(f"All defined labels with instruction indexes: {self.labels}", file=sys.stderr)
        print(f"The stack for returns: {self.calls_stack}", file=sys.stderr)
        print(f"Stack: {self.data_stack}", file=sys.stderr)
        self.dump_frames()

    function_table = {
        "MOVE": interpret_move,
        "CREATEFRAME": interpret_createframe,
        "PUSHFRAME": interpret_pushframe,
        "POPFRAME": interpret_popframe,
        "DEFVAR": interpret_defvar,
        "CALL": interpret_call,
        "RETURN": interpret_return,
        "PUSHS": interpret_pushs,
        "POPS": interpret_pops,
        "ADD": interpret_add,
        "SUB": interpret_sub,
        "MUL": interpret_mul,
        "IDIV": interpret_idiv,
        "LT": interpret_lt,
        "GT": interpret_gt,
        "EQ": interpret_eq,
        "AND": interpret_and,
        "OR": interpret_or,
        "NOT": interpret_not,
        "INT2CHAR": interpret_int2char,
        "STRI2INT": interpret_stri2int,
        "READ": interpret_read,
        "WRITE": interpret_write,
        "CONCAT": interpret_concat,
        "STRLEN": interpret_strlen,
        "GETCHAR": interpret_getchar,
        "SETCHAR": interpret_setchar,
        "TYPE": interpret_type,
        "JUMP": interpret_jump,
        "JUMPIFEQ": interpret_jumpifeq,
        "JUMPIFNEQ": interpret_jumpifneq,
        "EXIT": interpret_exit,
        "DPRINT": interpret_dprint,
        "BREAK": interpret_break
    }


arguments = ProgramArguments()
instruction_table = InstructionTable()
source_parser = SourceParser(arguments.source_path, instruction_table)
source_code = source_parser.program

interpreter = Interpreter(arguments.input_path)
interpreter.interpret_program(source_code)
