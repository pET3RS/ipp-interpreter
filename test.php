<?php

# This class serves the purpose of a enum for all the possible arguments which
# are stored in ScriptSettings class and are then adressed using this enum-like
# class
class ScriptArgs {
    const direct = "directory";
    const recursive = "recursive";
    const parse_script = "parse-script";
    const int_script = "int-script";
    const parse_only = "parse-only";
    const int_only = "int-only";
    const jexamxml = "jexamxml";
    const jexamcfg = "jexamcfg";
}


# This class stores the scritp settings which are set by the script arguments.
# It also contains few get and set methods which are quite self explenatory and
# at the same time bit unnecessary, but were used to get the hang of the OOP.
class ScriptSettings {
    public $binary_settings = array(
        ScriptArgs::recursive => 0,
        ScriptArgs::parse_only => 0,
        ScriptArgs::int_only => 0,
        ScriptArgs::direct => 0,
        ScriptArgs::parse_script => 0,
        ScriptArgs::int_script => 0,
        ScriptArgs::jexamxml => 0,
        ScriptArgs::jexamcfg => 0,

    );

    public $directory_settings = array(
        ScriptArgs::direct => ".",
        ScriptArgs::parse_script => "./parse.php",
        ScriptArgs::int_script => "./interpret.py",
        ScriptArgs::jexamxml => "/pub/courses/ipp/jexamxml/jexamxml.jar",
        ScriptArgs::jexamcfg => "/pub/courses/ipp/jexamxml/options",
    );

    public function getBinnarySetting($key) {
        return $this->binary_settings[$key];
    }

    public function setBinnarySetting($key) {
        $this->binary_settings[$key] = 1;
    }

    public function getDirectorySetting($key) {
        return $this->directory_settings[$key];
    }

    public function setDirecortySetting($key, $value) {
        if ( $key == ScriptArgs::direct ) {
            if (!is_dir($value)) {
            exit_with_error_message(41, "The specified directory ($value) isn't valid!");
            } else {
                # remove / at the end of directory if present
                $dir_arr = explode("/", $value);
                if ( strcmp(end($dir_arr),"") == 0 ) {
                    $value = substr_replace($value ,"", -1);
                }
            }
        } else {
            if ( ! is_readable($value) ){
                exit_with_error_message(41, "The specified file ($value) isn't readable!");
            }
        }
        $this->directory_settings[$key] = $value;
    }
}

$LONGARGS  = array(
    "help",
    "recursive",
    "parse-only",
    "int-only",
    "directory:",
    "parse-script:",
    "int-script:",
    "jexamxml:",
    "jexamcfg:"
);

$TEST_COUNTER = array(
    "TOTAL" => 0,
    "PASSED" => 0,
    "FAILED" => 0
);

# This function unifies the error messages. Prints specified error message on
# the STDERR and exits the program.
# Params:
#   $exit_val - integer that represents exit value
#   $error_message - string containing the error message
function exit_with_error_message($exit_val, $error_message) {
    fprintf(STDERR, "[E]: %s\n", $error_message);
    exit($exit_val);
}

# This function generates missing files that should be generated when they
# aren't present.
# Params:
#   $SETTINGS - ScriptSettings instance containing settings of the script
#   $dir - string representing directory path
#   $file_name - string containing name of the test
#   $files_arr - array of strings representing all files in the specified directory
function generate_missing_files($SETTINGS, $dir, $file_name, $files_arr){

    $files_to_generate = array(
        "$file_name.in",
        "$file_name.out",
        "$file_name.rc");
     
    if ($SETTINGS->getBinnarySetting(ScriptArgs::parse_only) == 1) {
        $files_to_generate = array("$file_name.out","$file_name.rc");
    }

    foreach ($files_to_generate as $file_to_generate) {
        if ( !in_array($file_to_generate, $files_arr) ) {
            $open_file = fopen("$dir/$file_to_generate", "w");
            if ( $open_file === false ) {
                exit_with_error_message(11, "Failed to create a missing test file (.rc|.out|.in)!");
            }
            if ( strcmp($file_to_generate, "$file_name.rc") == 0 ) {
                if ( fwrite($open_file, "0") === false ) {
                    fclose($open_file);
                    exit_with_error_message(11, "Failed to write to .rc file after its creation!");
                }
                
            }
            fclose($open_file);
        }
    }
}

# --- HTML output auxiliary functions ---

# This function adds a row to the table containing test status and test file
# Params:
#   $doc - DOMDocument one is using
#   $table_main_body_row - instance of DOMElement refering to tables body
#   $status - string containing [FAILED] or [PASSED] values depending on the 
#             tests success
#   $file - string representing the test name including its path
#   $info - the info about the tests outcome - this is used when one howers over
#           the tests status to see more information about what did fail.
function create_td($doc, $table_main_body_row, $status, $file, $info) {
    $table_main_body_row_td = $doc->createElement("td");
    $table_main_body_row_td = $table_main_body_row->appendChild($table_main_body_row_td);

    $table_main_body_row_td_a = $doc->createElement("a", $status);
    $table_main_body_row_td_a_attr1 = $doc->createAttribute("href");
    $table_main_body_row_td_a_attr2 = $doc->createAttribute("class");
    $table_main_body_row_td_a_attr3 = $doc->createAttribute("title");
    $table_main_body_row_td_a_attr1->value=" ";
    if ( strcmp($status, "[PASSED]") == 0 ) {
        $table_main_body_row_td_a_attr2->value="passed";
    } else {
        $table_main_body_row_td_a_attr2->value="failed";
    }
    $table_main_body_row_td_a_attr3->value=$info;
    $table_main_body_row_td_a->appendChild($table_main_body_row_td_a_attr1);
    $table_main_body_row_td_a->appendChild($table_main_body_row_td_a_attr2);
    $table_main_body_row_td_a->appendChild($table_main_body_row_td_a_attr3);

    $table_main_body_row_td->appendChild($table_main_body_row_td_a);

    $table_main_body_row_td = $doc->createElement("td", $file);
    $table_main_body_row_td = $table_main_body_row->appendChild($table_main_body_row_td);
}

# This function creates and adds an attribute to an element of DOMDocument and
# can also add that element to its parent element if the $parent is specified.
# If you chode to not include parent please set it to false!
# Params:
#   $doc - DOMDocument one is using
#   $element - the DOMElement one wants to add the attribute to
#   $name - string representing the name of the new attribute
#   $value - string representing the value of the new attribute
#   $parent - either DOMElement that is the parent of the $element or false
# Returns:
#   DOMElement of the $parent if specified or the $element
function create_and_add_attribute($doc, $element, $name, $value, $parent) {
    $element_attr = $doc->createAttribute($name);
    $element_attr->value = $value;
    $element->appendChild($element_attr);
    if ( $parent !== false ) {
        return $parent->appendChild($element);
    } else {
        return $element;
    }
}

# This function creates a new DOMElement in a DOMDocument. It can also append
# the element as a child to its parent if it is specified. If you dont want to
# specify the parent set it to false!
# Params:
#   $doc - DOMDocument one is using
#   $name - string representing name of the new element
#   $value - string representing value of the new element
#   $parent - DOMElement of the parent of the new element or false
# Returns:
#   DOMElement of the parent if specified otherwise DOMElement of $element
function create_element($doc, $name, $value="", $parent) {
    if ( strcmp($value, "") == 0 ) {
        $element = $doc->createElement($name);
    } else {
        $element = $doc->createElement($name, $value);
    }
    if ( $parent !== false ) {
        return $parent->appendChild($element);
    } else {
        return $element;
    }
}
# --- End of HTML creation auxiliary functions ---

# This function creates file with unique name in the specified directory with
# the specified prefix in the name. If file creation fails function exits the
# program!
# Params:
#   $dir - string containing the path where the file should be created
#   $prefix - string containing prefix of the file name
# Returns:
#   string - path to created file
function create_temp_file_with_unique_name($dir, $prefix) {
    $tmp_file = tempnam($dir, $prefix);
    #FIXME false if fail
    $tmp_fh = fopen($tmp_file, "w");
    if ( $tmp_fh === false ) {
        #FIXME err code
        exit_with_error_message(99, "Temporary file creation failed!");
    }
    fclose($tmp_fh);
    return $tmp_file;
}

# This function exits the program if a file from the specified array of files 
# isn't readable or doesn't exist.
# Params:
#   $file_arr - array of strings representing file patshs that one wants to check
#               for existance and readability
#   $exit_val - integer representing exit value if a file doesn't exist or
#               isn't readable
#   $error_message - string containing error message that will be printed
#                    on STDERR
function exit_if_not_readable($files_arr, $exit_val, $error_message) {
    foreach ($files_arr as $file) {
        if ( ! is_readable($file) ){
            exit_with_error_message($exit_val, $error_message);
        }
    }
}


# This function reads a return code from reference file and returns it. On any
# error exits the program.
# Params:
#   $return_code_file - string containing the file path
# Returns:
#   the return code from the provided file
function get_return_code_from_file($return_code_file){
    # Get the return code from reference file
    $return_cofe_fh = fopen($return_code_file, "r");
    if ( $return_code_file === false ) {
        exit_with_error_message(11, "Failed to open the $return_code_file file");
    }
    else {
        $return_code = (int)fread($return_cofe_fh, filesize($return_code_file));
        if ( $return_code === false ) {
            exit_with_error_message(11, "Failed to read the $return_code_file file");
        }
    }
    fclose($return_cofe_fh);
    return $return_code;
}

# This function runs the parser tests. Runs the specified parser script, then
# evaluates its return codes and decides if the test passed/failed or shall
# continue. If it continues the output of parser is stored in a temporary file
# and then compared with a reference file. The function creates 2 temporary
# files - one for the parser output as mentioned and the other as a dump file 
# for delta of the JExamXML tool that compares the XML output of parser with 
# reference file. On any error exits the program.
# Params:
#   $SETTINGS - ScriptSettings instance containing settings of the test.php
#   $dir - string representing the current dirrectory in which is the test 
#          located.
#   $file_name - the name of the test that's being executed.
#   $doc - DOMDocument instance containing the table in which the result is 
#          written
#   $table_main_body_row - DOMElement the row where should be the result
#                          written
function parser_only_test($SETTINGS, $dir, $file_name, $doc, $table_main_body_row){

    # Reference files
    $src_file = "$dir/$file_name.src";
    $xml_file= "$dir/$file_name.out";
    $return_code_file = "$dir/$file_name.rc";

    exit_if_not_readable([$src_file, $xml_file, $return_code_file],
                         11, "One of test files ($dir/$file_name) isn't readable!");

    # Get paths to files needed for this test
    $parse_script = $SETTINGS->getDirectorySetting(ScriptArgs::parse_script);
    $jexamxml = $SETTINGS->getDirectorySetting(ScriptArgs::jexamxml);
    $jexamcfg = $SETTINGS->getDirectorySetting(ScriptArgs::jexamcfg);
    
    exit_if_not_readable([$parse_script, $jexamcfg, $jexamxml], 11,
                         "At least one of the required files ($parse_script or $jexamxml or $jexamcfg) isn't readable!");
    
    # Get the return code from reference file
    $return_code = get_return_code_from_file($return_code_file);

    # Create file with unique name for parser output
    $xml_out_tmp_file = create_temp_file_with_unique_name(".", "PARSER_TEST_xml_output");
    $fh = fopen($xml_out_tmp_file, "w");
    if ( $fh === false ) {
        exit_with_error_message(12, "Failed to create a temporary file!");
    }
    fclose($fh);

    # Run the parse script using $src_file
    $parser_exec = exec("php7.4 $parse_script < $src_file 2>/dev/null 1>$xml_out_tmp_file", $parser_xml_out, $parser_return_code);
    if ( $parser_exec === false ) {
        unlink($xml_out_tmp_file);
        exit_with_error_message(99, "Function exec() failed while trying to run $parse_script!");
    }

    # Test if the parser matches the expected results determined by the reference files
    
    if ( $parser_return_code != 0 ) {
        # The parser failed => compare the return values and decide if test
        # passed or not and end the test
        if ( $parser_return_code == $return_code ) {
            unlink($xml_out_tmp_file);
            create_td($doc, $table_main_body_row, "[PASSED]", "$dir/$file_name",
                      "Parser fails, but return codes match!");
            return 0;
        } else {
            unlink($xml_out_tmp_file);
            create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                      "Parser fails and return codes don't match!");
            return 1;
        }
    }       

    # The parser evaluated the program to be correct and generated xml
    # output => save it to the file and compare using JExamXML program to
    # decide if it is correct or not and end the test.
    
    # Check if return codes match first to avoid unnecessary creation of files
    if ( $return_code != $parser_return_code ) {
        unlink($xml_out_tmp_file);
        create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                  "Parser success, but return codes don't match!");                 
        return 1;
    }

    # Create file with unique name for the delta of JExamXML tool
    $jexam_delta_tmp_file = create_temp_file_with_unique_name(".", "PARSER_TEST_jexam_delta");

    # Run the xml comparing tool to decide whether the output matches the reference file
    $command =  "java -jar $jexamxml $xml_out_tmp_file $xml_file $jexam_delta_tmp_file $jexamcfg | grep identical";
    $jexamxml_exec = exec($command, $identical);
    unlink($jexam_delta_tmp_file); # Delete the unnecessary delta file
    unlink($xml_out_tmp_file);
    if ( $jexamxml_exec === false ) {
        exit_with_error_message(99, "Failed to execute JExamXML!");
    }
    
    # Convert the output of the exec() to a string
    $identical = implode(" ", $identical);
    
    # Validate the output of the xml comparing tool
    if ( strcmp($identical,"Two files are identical") == 0) {
        create_td($doc, $table_main_body_row, "[PASSED]", "$dir/$file_name", 
                  "Parser success, return codes and outputs match!");
        return 0;
    } else {
        create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                  "Parser success and return codes match, but outputs don't match!");
        return 1;
    }
}

# This function tests the interpret only. First the interpret is run and its
# return codes are evaluated. If the returncodes match a temporary file is
# created and filled with output of the interpret and after that the output of
# the interpret is compared to the reference file using diff. On any error the
# function exits the program.
# Params:
#   $SETTINGS - ScriptSettings instance containing settings of the test.php
#   $dir - string representing the current dirrectory in which is the test 
#          located.
#   $file_name - the name of the test that's being executed.
#   $doc - DOMDocument instance containing the table in which the result is 
#          written
#   $table_main_body_row - DOMElement the row where should be the result
#                          written
# Returns:
#   0 - on the test success
#   1 - on the test failure
function interpret_only_test($SETTINGS, $dir, $file_name, $doc, $table_main_body_row) {

    # Reference files
    $src_file = "$dir/$file_name.src";
    $in_file = "$dir/$file_name.in";
    $out_file= "$dir/$file_name.out";
    $return_code_file = "$dir/$file_name.rc";

    exit_if_not_readable([$src_file, $in_file, $out_file, $return_code_file],
                         11, "Couldn't read at least one of test files ($dir/$file_name) isn't readable!");

    $return_code = get_return_code_from_file($return_code_file);
    
    # Get paths to files needed for this test
    $int_script = $SETTINGS->getDirectorySetting(ScriptArgs::int_script);
    exit_if_not_readable([$int_script], 11, "Couldn't read $int_script!");

    # Create temporary file for interpreter output
    $int_out_tmp_file = create_temp_file_with_unique_name(".", "INTERPRETER_TEST_output");
    $fh = fopen($int_out_tmp_file, "w");
    if ( $fh === false ) {
        exit_with_error_message(12, "Failed to create a temporary file!");
    }
    fclose($fh);

    # Run the interpreter script
    $command = "python3.8 $int_script --source=$src_file --input=$in_file 2>/dev/null 1>$int_out_tmp_file";
    $int_exec = exec($command, $int_output, $int_return_code);
    if ( $int_exec === false ) {
        unlink($int_out_tmp_file);
        exit_with_error_message(99, "Function exec() failed to execute $int_script!");
    }

    # Validate the return code
    if ( $int_return_code != 0 ) {
        unlink($int_out_tmp_file);
        if ( $int_return_code == $return_code ) {
            create_td($doc, $table_main_body_row, "[PASSED]",
                "$dir/$file_name",
                "Interpret fails, but return codes match!");
            return 0;
        } else {
            create_td($doc, $table_main_body_row, "[FAILED]",
                "$dir/$file_name",
                "Interpret fails and return codes don't match! INTERPRET: $int_return_code EXPECTED: $return_code");
            return 1;
        }
    }
    if ( $return_code != $int_return_code ) {
        unlink($int_out_tmp_file);
        create_td($doc, $table_main_body_row, "[FAILED]",
            "$dir/$file_name",
            "Interpret success, but return codes don't match! INTERPRET: $int_return_code EXPECTED: $return_code");
        return 1;
    }
    
    # Validate output
    $command = "diff -qs $int_out_tmp_file $out_file";
    $diff_exec = exec($command, $diff_output, $diff_return_code);
    unlink($int_out_tmp_file);
    if ( $diff_exec === false ) {
        exit_with_error_message(99, "Function exec() failed to execute diff!");
    }

    if ( $diff_return_code == 0 ) {
        create_td($doc, $table_main_body_row, "[PASSED]", 
            "$dir/$file_name",
            "Interpret success, return codes and outputs match!");
        return 0;
    } else {
        create_td($doc, $table_main_body_row, "[FAILED]", 
            "$dir/$file_name",
            "Interpret success and return codes match, but outputs don't match!");
        return 1;
    }
}

# This function tests both a parser and a interpret in a pipelike action. First 
# the interpret is executed an its return code evaluated. If the parser fails 
# the test is marked as failed. If it doesn't the parser output is stored in a
# temporary file which is then used as input for the interpreter upon its
# execution. After the interpreter execution function evaluates its return code
# and if needed creates a temporary file for the interpret output and compares
# it to reference file using diff. On any error the function exits the program.
# Params:
#   $SETTINGS - ScriptSettings instance containing settings of the test.php
#   $dir - string representing the current dirrectory in which is the test 
#          located.
#   $file_name - the name of the test that's being executed.
#   $doc - DOMDocument instance containing the table in which the result is 
#          written
#   $table_main_body_row - DOMElement the row where should be the result
#                          written 
# Returns:
#   0 - on the test success
#   1 - on the test failure
function parser_interpret_test($SETTINGS, $dir, $file_name, $doc, $table_main_body_row) {
    # Reference files
    $src_file = "$dir/$file_name.src";
    $in_file = "$dir/$file_name.in";
    $out_file= "$dir/$file_name.out";
    $return_code_file = "$dir/$file_name.rc";

    exit_if_not_readable([$src_file, $in_file, $out_file, $return_code_file],
                         11, "Couldn't read at least one of test files ($dir/$file_name) isn't readable!");

    # Get the return code from reference file
    $return_code = get_return_code_from_file($return_code_file);

    # Get paths to files needed for this test
    $parse_script = $SETTINGS->getDirectorySetting(ScriptArgs::parse_script);
    $int_script = $SETTINGS->getDirectorySetting(ScriptArgs::int_script);
    $jexamxml = $SETTINGS->getDirectorySetting(ScriptArgs::jexamxml);
    $jexamcfg = $SETTINGS->getDirectorySetting(ScriptArgs::jexamcfg);
    
    exit_if_not_readable([$parse_script, $jexamcfg, $jexamxml, $int_script], 11,
                         "At least one of the required files ($parse_script or $int_script or $jexamxml or $jexamcfg) isn't readable!");

    # Create temporary file for parser output
    $xml_out_tmp_file = create_temp_file_with_unique_name(".", "BOTH_TEST_parser_output");
    $fh = fopen($xml_out_tmp_file, "w");
    if ( $fh === false ) {
        exit_with_error_message(12, "Failed to create a temporary file!");
    }
    fclose($fh);

    # Execute the parser script and evaluate its results
    $command = "php7.4 $parse_script < $src_file 2>/dev/null 1>$xml_out_tmp_file";
    $parser_exec = exec($command, $parser_output, $parser_return_code); 
    if ( $parser_exec === false ) {
        unlink($xml_out_tmp_file);
        exit_with_error_message(99, "Function exec() failed to execute $parse_script!");
    }

    # Validate parser return code
    if ( $parser_return_code != 0 ) {
        unlink($xml_out_tmp_file);
        create_td($doc, $table_main_body_row, "[FAILED]",
                  "$dir/$file_name", "Parser failed! RETURNED: $parser_return_code");
        return 1;
    }

    #Create temporary file for interpreter output
    $int_out_tmp_file = create_temp_file_with_unique_name(".", "BOTH_TEST_interpret_output");
    $fh = fopen($int_out_tmp_file, "w");
    if ( $fh === false ) {
        exit_with_error_message(12, "Failed to create a temporary file!");
    }
    fclose($fh);

    # Run interpret
    $command = "python3.8 $int_script --source=$xml_out_tmp_file --input=$in_file 2>/dev/null 1>$int_out_tmp_file";
    $int_exec = exec($command, $int_output, $int_return_code); 
    unlink($xml_out_tmp_file); # No longer need this tmp file
    if ( $int_exec === false ) {
        unlink($int_out_tmp_file);
        exit_with_error_message(99, "Function exec() failed to execute $int_script!");
    }

    # Validate interpret return code
    if ( $int_return_code != 0 ) {
        unlink($int_out_tmp_file);
        if ( $int_return_code == $return_code ) {
            create_td($doc, $table_main_body_row, "[PASSED]", "$dir/$file_name",
                      "Interpret fails, but return codes match!");
            return 0;
        } else {
            create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                      "Interpret fails and return codes don't match! INTERPRET: $int_return_code EXPECTED: $return_code");
            return 1;
        }               
    }
    if ( $int_return_code != $return_code ) {
        unlink($int_out_tmp_file);
        create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                  "Interpreter success, but return codes don't match! INTERPRET: $int_return_code EXPECTED: $return_code");
        return 1;
    }

    # Validate interpret output
    $command = "diff -qs $int_out_tmp_file $out_file";
    $diff_exec = exec($command, $diff_output, $diff_return_code);
    unlink($int_out_tmp_file);
    if ( $diff_exec === false ) {
        exit_with_error_message(99, "Function exec() failed to execute diff!");
    }


    if ( $diff_return_code == 0 ) {
        create_td($doc, $table_main_body_row, "[PASSED]", "$dir/$file_name",
                  "Interpret success, return codes and outputs match!");
        return 0;
    } else {
        create_td($doc, $table_main_body_row, "[FAILED]", "$dir/$file_name",
                  "Interpret success and return codes match, but outputs don't match!");
        return 1;
    }
}

# This function scans the specified directory, runs the tests and appends their
# results to the output html.
# Params:
#   $directory - string representing the directory containing the tests
#   $SETTINGS - ScriptSettings instance containing scripts settings
#   &$TEST_COUNTER - reference to an array containing counters of the tests
#   $doc - DOMDocument instance containing the html output
#   $table_main_body - DOMElement to which should be the results of the test 
#                      table appended
function scan_dir_and_run_tests($directory, $SETTINGS, &$TEST_COUNTER, $doc, $table_main_body) {

    $scanned_directory = array_diff(scandir($directory), array('..', '.'));
    foreach ($scanned_directory as $file) {

        if ( !is_dir($directory . "/" . $file) ) {
            $is_src = preg_match("/\.src$/", $file);

            if ( $is_src === false ) {
                exit_with_error_message(99, "Function preg_match failed!");
            }
            elseif ( $is_src == 1 ) {
                 
                $file_name = preg_replace("/\.src$/", "", $file);
                if ( $file_name == null ) {
                    exit_with_error_message(99, "Function preg_replace failed!");
                }

                generate_missing_files($SETTINGS, $directory, $file_name, $scanned_directory);
                
                # Creation of a row in the HTML output table containing test results
                $table_main_body_row =create_element($doc, "tr", "", $table_main_body);

                if ( $SETTINGS->getBinnarySetting(ScriptArgs::parse_only ) == 1 ) {
                    $outcome = parser_only_test($SETTINGS, $directory, $file_name, $doc, $table_main_body_row);
                }
                elseif ($SETTINGS->getBinnarySetting(ScriptArgs::int_only) == 1 ) {
                    $outcome = interpret_only_test($SETTINGS, $directory, $file_name, $doc, $table_main_body_row);
                } else {
                    $outcome = parser_interpret_test($SETTINGS, $directory, $file_name, $doc, $table_main_body_row);
                }

                if ($outcome == 1) {
                    $TEST_COUNTER["FAILED"] += 1;
                } else {
                    $TEST_COUNTER["PASSED"] += 1;
                }
                $TEST_COUNTER["TOTAL"] += 1;
                
            }
        }
        elseif ( $SETTINGS->getBinnarySetting(ScriptArgs::recursive) == 1 ) {
            scan_dir_and_run_tests($directory . "/" . $file, $SETTINGS, $TEST_COUNTER, $doc, $table_main_body);
        }
    }
}

# This function creates the description text for the parser output html site.
# Params:
#   $SETTINGS - ScriptSettings instance containing scripts settings
# Returns:
#   string containing the description
function create_mode_description($SETTINGS) {

    $parse_script = $SETTINGS->getDirectorySetting(ScriptArgs::parse_script);
    $interpret_script = $SETTINGS->getDirectorySetting(ScriptArgs::int_script);

    if ( $SETTINGS->getBinnarySetting(ScriptArgs::parse_only) == 1 ) {
        $mode = "parser only. The test.php used " . $parse_script . " version of the script.";
    }
    elseif ( $SETTINGS->getBinnarySetting(ScriptArgs::int_only) == 1 ) {
        $mode = "interpreter only. The test.php used " . $interpret_script . " version of the script.";
    } else {
        $mode = "both parser and interpreter. The test.php used " . $parse_script . " version of the parse script and " . $interpret_script . " version of the interpret script." ;
    }

    $description = "Tests were run in mode which tests " . $mode;
    
    return $description;

}

# --- Arguments handling ---

$args_end_index = null;
$args = getopt("", $LONGARGS, $args_end_index);

$post_args = array_slice($argv, $args_end_index);
if ( count($post_args) > 0 ) {
    exit_with_error_message(10, "Incorrect use of arguments");
}

$SETTINGS = new ScriptSettings();

foreach ($args as $argument => $value) {

    if ( $argument == "help" ) {
        if ( $argc != 2 ) {
            exit_with_error_message(10,"Incorrect use of --help argument");
        }
        printf("This is tester for IPP project 21!\n".
               "Supported arguments:\n".
               "\t--directory=<path>   \t specifies location of the tests [default=.]\n".
               "\t--recursive          \t search in the specified directory recursively\n".
               "\t--parse-only         \t tests the parser script only\n".
               "\t--parse-script=<path>\t specifies location of the parser script that should be used [default=./parse.php]\n".
               "\t--int-only           \t tests the interpret script only\n".
               "\t--int-script=<path>  \t specifies location of the interpret script that should be tested [default=./interpret.php]\n".
               "\t--jexamxml=<path>    \t specifies location of the JExamXML tool for comparing parser outputs [default=/pub/courses/ipp/jexamxml/jexamxml]\n".
               "\t--jexamcfg=<path>    \t specifies locatio of the JExamXML tool configuration file [default=/pub/courses/ipp/jexamxml/options]\n");
        exit(0);
    }
    elseif ( $argument != ScriptArgs::recursive  && 
             $argument != ScriptArgs::parse_only && 
             $argument != ScriptArgs::int_only      ) {
        
        $SETTINGS->setDirecortySetting($argument, $value);
    } 
    $SETTINGS->setBinnarySetting($argument);

}

$parse_only = $SETTINGS->getBinnarySetting(ScriptArgs::parse_only) == 1;
$parse_script = $SETTINGS->getBinnarySetting(ScriptArgs::parse_script) == 1;
$int_only = $SETTINGS->getBinnarySetting(ScriptArgs::int_only) == 1;
$int_script = $SETTINGS->getBinnarySetting(ScriptArgs::int_script) == 1;

if ( ($parse_only && ($int_only || $int_script)) || ($int_only && ($parse_only || $parse_script)) ) {
    exit_with_error_message(10, "Incorrect use of arguments!");
}

# --- Arguments handling end ---


# --- Creation of HTML output ---
$doc = new DOMDocument();

printf("<!DOCTYPE html>\n");

$root = create_element($doc, "html", "", $parent=$doc);

$head = create_element($doc, "head", "", $root);
$meta = create_element($doc, "meta", "", $head);
create_and_add_attribute($doc, $meta, "charset", "utf-8", $parent=false);
$title = create_element($doc, "title", "test.php results", $head);
$css_text  = "\np {\ncolor:#ebdbb2;\nwidth: 750px}\n";
$css_text .= "body {\nbackground-color:#1d2021}\n";
$css_text .= "td {\npadding:5px;}\n";
$css_text .= "h1 {\nmargin-top:30px;\nmargin-bottom:60px;\ncolor:#458588;}\n";
$css_text .= "h3 {\nline-heigth:0px;\ncolor:#83a598;}\n";
$css_text .= "table {\ncolor:#ebdbb2;}\n";
$css_text .= ".passed {\ncolor:#b8bb26;\ntext-decoration:none;}\n";
$css_text .= ".failed {\ncolor:#fb4934;\ntext-decoration:none;}\n";
create_element($doc, "style", $css_text, $head);

$body = create_element($doc, "body", "", $root);
$center = create_element($doc, "center", "", $body);
create_element($doc, "h1", "IPP - test.php results", $center);
create_element($doc, "h3", "Mode description", $center);
create_element($doc, "p", create_mode_description($SETTINGS), $center);
create_element($doc, "br", "", $center);
create_element($doc, "p", "TIP: Hovering over test status will show additional info.", $center);
create_element($doc, "br", "", $center);

#begin results table
$table_results = create_element($doc , "table", "", $center);
create_and_add_attribute($doc, $table_results, "border", "2", $parent=false);
$table_results_body = create_element($doc, "tbody", "", $table_results);
$table_results_body_row_passed = create_element($doc, "tr", "", $table_results_body);
$table_results_body_row_td = create_element($doc, "td", "[PASSED]", $table_results_body_row_passed);
create_and_add_attribute($doc, $table_results_body_row_td, "class", "passed", $table_results_body_row_passed);
$table_results_body_row_failed = create_element($doc, "tr", "", $table_results_body);
$table_results_body_row_td = create_element($doc, "td", "[FAILED]", $table_results_body_row_failed);
create_and_add_attribute($doc, $table_results_body_row_td, "class", "failed", $parent=false);

$table_results_body_row_total = create_element($doc, "tf", "", $table_results_body);
create_element($doc, "td", "TOTAL", $table_results_body_row_total);
#end of results table

create_element($doc, "br", "", $center);

# begin main table
$table_main = create_element($doc, "table", "", $center);
create_and_add_attribute($doc, $table_main, "border", "2", $parent=false);
$table_main_head = create_element($doc, "thead", "", $table_main);
$table_main_head_row = create_element($doc, "tr", "", $table_main_head);
$table_main_head_row_th = create_element($doc, "th", "Status", $table_main_head_row);
create_and_add_attribute($doc, $table_main_head_row_th, "scope", "col", $parent=false);
$table_main_head_row_th = create_element($doc, "th", "Test file", $table_main_head_row);
create_and_add_attribute($doc, $table_main_head_row_th, "scope", "col", $parent=false);
$table_main_body = create_element($doc, "tbody", "", $table_main);
# main table continues its creation while tests are run
# --- End of creation of HTML output ---

# --- Run tests ---
$dir = $SETTINGS->getDirectorySetting(ScriptArgs::direct);
scan_dir_and_run_tests($dir, $SETTINGS, $TEST_COUNTER, $doc, $table_main_body);
# --- End of run tests ---

# --- Finish HTML output creation ---
# After we run the tests the main table is filled and we can procced 
# with creation of final results table.

# finalisation of results table
create_element($doc, "td", $TEST_COUNTER["PASSED"], $table_results_body_row_passed);
create_element($doc, "td", $TEST_COUNTER["FAILED"], $table_results_body_row_failed);
create_element($doc, "td", $TEST_COUNTER["TOTAL"], $table_results_body_row_total);
echo $doc->saveHTML();
# --- End of finish HTML output creation ---
?>
