<?php 
ini_set('display_errors', 'stderr');

# This class contains all possible operand types.
# Purpose is same as enum in language C. 
class OperandTypes {
    const variable = 1;
    const symbol = 2;
    const label = 3;
    const type = 4;
}

# This class represents expected operands of a instruction,
# When creating new instance specify in an array all the types instruction 
# expects (for this purpose see OperandTypes class).
# Purpose of this class is to provide sufficient information about expected
# operands in an instruction.
class Operands {
    public $operands_count;
    public $operands_types;

    function __construct($types_arr) {
        $this->operands_count = count($types_arr);
        $this->operands_types = $types_arr;
    }
}

#Table containing information about IPPcode21
$INSTRUCTION_TABLE = array(
    "MOVE" => new Operands(array(OperandTypes::variable, OperandTypes::symbol)),
    "CREATEFRAME" => new Operands(array()), 
    "PUSHFRAME" => new Operands(array()),
    "POPFRAME" => new Operands(array()),
    "DEFVAR" => new Operands(array(OperandTypes::variable)), 
    "CALL" => new Operands(array(OperandTypes::label)),
    "RETURN" => new Operands(array()),

    "PUSHS" => new Operands(array(OperandTypes::symbol)),
    "POPS" => new Operands(array(OperandTypes::variable)),

    "ADD" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "SUB" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "MUL" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "IDIV" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "LT" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "GT" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "EQ" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "AND" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "OR" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "NOT" => new Operands(array(OperandTypes::variable, OperandTypes::symbol)),
    "INT2CHAR" => new Operands(array(OperandTypes::variable, OperandTypes::symbol)),
    "STRI2INT" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),

    "READ" => new Operands(array(OperandTypes::variable, OperandTypes::type)),
    "WRITE" => new Operands(array(OperandTypes::symbol)),

    "CONCAT" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "STRLEN" => new Operands(array(OperandTypes::variable, OperandTypes::symbol)),
    "GETCHAR" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),
    "SETCHAR" => new Operands(array(OperandTypes::variable, OperandTypes::symbol, OperandTypes::symbol)),

    "TYPE" => new Operands(array(OperandTypes::variable, OperandTypes::symbol)),

    "LABEL" => new Operands(array(OperandTypes::label)),
    "JUMP" => new Operands(array(OperandTypes::label)),
    "JUMPIFEQ" => new Operands(array(OperandTypes::label, OperandTypes::symbol, OperandTypes::symbol)),
    "JUMPIFNEQ" => new Operands(array(OperandTypes::label, OperandTypes::symbol, OperandTypes::symbol)),
    "EXIT" => new Operands(array(OperandTypes::symbol)),

    "DPRINT" => new Operands(array(OperandTypes::symbol)),
    "BREAK" => new Operands(array())
);

define("HEADER", ".IPPcode21");

# This function takes only part of specified string before #.
# Takes the # character as a unique marker for a comment.
# Be aware that this function exits the program when substr function fails!
# params:
#   $line - string in which one wants to remove comments
function remove_comments($line) {
    for ( $idx = 0; $idx < strlen($line); $idx++ ) {
        if ( strcmp($line[$idx], '#') == 0 ) {
            $line = substr($line, 0, $idx);
            if ( $line === false ) {
                print_error_and_exit(99, "Function substr failed!");
            }
        }
    }
    return $line;
}

# This function takes single line from STDIN and divides it to an array based on
# valid divisors from the assignment. Also function ignores comments on the same
# line and commented out lines. If there is EOF on STDIN returns false otherwise
# the array containing the line from STDIN it processes.
function get_line_as_array() {
    $line = fgets(STDIN);
    if ( $line == false ) {
        return false;
    }
    $line = remove_comments($line);
    
    # Simplifies the line by replacing whitespace sequences with single space
    $line = preg_replace("/[\s\t]+/", ' ', trim($line));

    # line is commented out or is empty
    if ( ($line == '') || ($line == ' ') ) {
        return get_line_as_array();
    }
   
    $line = explode(' ', $line);

    return $line;
}

# This function prints error message specified and exits the program with specified exit value.
# params:
#   $exit_val - int with which the program should exit
#   $error_message - string that should be printed as an error message on STDERR
#   $instruction_number - int that represents the serial number of the instruction that caused the error
#   $operand_number - int that represents the operand that caused the error
function print_error_and_exit($exit_val, $error_message, $instruction_number=0, $operand_number=0) {
    if ( $operand_number == 0 && $instruction_number == 0 ) {
        fprintf(STDERR, "[E]: %s\n", $error_message);
    } 
    elseif ( $instruction_number != 0 && $operand_number == 0 ) {
        fprintf(STDERR, "[E]: %s (instruction number: %d)\n", $error_message, $instruction_number);
    } else {
        fprintf(STDERR, "[E]: %s (instruction number: %d, operand: %d)\n", $error_message, $instruction_number, $operand_number);
    }
    exit($exit_val);
}

# This function creates an xml argument and is used to complete the xml output.
# params:
#   $xml_writer - object of class XMLWriter 
#   $operand_number - int the order number of operand
#   $operand_type - string that represents the type of operand
#   $operand_contents - string that represents the value held by the argument
function create_xml_arg($xml_writer, $operand_number, $operand_type, $operand_contents) {
    $xml_writer->startElement("arg".$operand_number);
    $xml_writer->startAttribute("type");
    $xml_writer->text((string)$operand_type);
    $xml_writer->endAttribute();
    if ( strcmp($operand_contents, "") != 0 ) {
        $xml_writer->text((string)$operand_contents);
    }
    $xml_writer->endElement();
}


# Handles the arguments
# For now there is only one
foreach ($argv as $arg) {
    switch ($arg) {
        case "--help":
            if ( $argc != 2 ) {
                print_error_and_exit(10,"Incorrect use of --help option");
            }
            printf("This is parser for the IPP project 2021!\n".
                   "\nRecommended usage:\n".
                   "\tphp ./parse.php < IPPcode21_source_file\n".
                   "This will print xml representation of the IPPcode21 code".
                   " to the STDOUT.\n".
                   "\nSuported arguments:\n".
                   "\t --help \t prints this help message\n");
            exit(0);
    }
}

# Validate the mandatory header (.IPPcode21) line.
$header = get_line_as_array();
if ( ($header == false) || (strcasecmp($header[0], HEADER) != 0) || (count($header) != 1) ) {
    exit(21);
}

# Start creation of XML output
$xmlw = new XMLWriter();
$xmlw->openMemory();
$xmlw->setIndentString(" ");
$xmlw->setIndent(true);
$xmlw->startDocument('1.0', 'UTF-8');
$xmlw->startElement("program");
$xmlw->startAttribute("language");
$xmlw->text("IPPcode21");
$xmlw->endAttribute();

# Load instructions one by one and generate XML output.
$instruction_counter = 0;
while( ($line = get_line_as_array()) != false ) {
    $instruction_counter++; 
    $current_opcode = $line[0];

    # Check if the opcode is valid and get information about its operands 
    $valid_opcode = false;
    foreach ( $INSTRUCTION_TABLE as $opcode => $operands_info ) {
        if ( strcasecmp($opcode, $current_opcode) == 0 ) {
            $valid_opcode = true;
            $current_operands_info = $operands_info;
            $current_opcode = $opcode; #forces the uppercase in future use
        }
    }
    if ( !$valid_opcode ) {
        print_error_and_exit(22,"Unknown instruction!", $instruction_counter);
    }

    # Check if number of expected operands is matched
    if ( $current_operands_info->operands_count != count($line)-1 ) {
        printf("%d == %d\n", $current_operands_info->operands_count, count($line)-1 );
        print_error_and_exit(23, "Incorrect number of operands", $instruction_counter);
    }

    #<instruction order="number" opcode="OPCODE">
    $xmlw->startElement("instruction");
    $xmlw->startAttribute("order");
    $xmlw->text((string)$instruction_counter);
    $xmlw->endAttribute();
    $xmlw->startAttribute("opcode");
    $xmlw->text($current_opcode);
    $xmlw->endAttribute();  

    # Check if operands match the types they are expected to and generate XML for them
    foreach ( $current_operands_info->operands_types as $idx => $expected_type ) {
        $current_operand = $line[$idx+1];
        $current_operand_postion = $idx+1;

        $variable_pattern = "/^(GF|LF|TF)@[a-zA-Z_\-$%&*?!][0-9a-zA-Z_\-$%&*?!]*$/";
        $label_pattern = "/^[a-zA-Z_\-$%&*?!][0-9a-zA-Z_\-$%&*?!]*$/";
        $type_pattern = "/^(int|bool|string)$/";
        $bool_pattern = "/^bool@(true|false)$/";
        $symbol_pattern_type_part = "/^(int|bool|string|nil)@/";

        switch ($expected_type) {
            case OperandTypes::symbol:
                $validity = preg_match($symbol_pattern_type_part, $current_operand);

                if ( $validity === false  ) {
                    print_error_and_exit(99, "Function preg_match failed!", $instruction_counter, $current_operand_postion);
                }
                elseif ( $validity == 1 ) { # the prefix matched so we continue further
                    # Now we can explode() the operand to two parts because we know that the type part is valid
                    $current_operand_parts = explode('@', $current_operand, 2);
                    #printf("current_operand: _%s_\n part1: _%s_\n part2: _%s_\n", $current_operand, $current_operand_parts[0], $current_operand_parts[1]);
                    switch ( $current_operand_parts[0] ) {
                        case "int":
                            if ( strcmp($current_operand_parts[1],"") == 0 ) {
                                print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                            }
                            break;
                        case "bool":
                            $validity = preg_match($bool_pattern, $current_operand);
                            if ( $validity === false ) {
                                print_error_and_exit(99, "Function preg_match failed!", $instruction_counter, $current_operand_postion);
                            }
                            elseif ( $validity == 0 ) {
                                print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                            }
                            break;
                        case "nil":
                            if ( strcmp("nil", $current_operand_parts[1]) != 0 ) {
                                print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                            }
                            break;
                        case "string":
                            $string = $current_operand_parts[1];
                            if ( strcmp($string, "") == 0 ) {
                                break;
                            }
                            $string_lenght = strlen($string);

                            if ( !mb_check_encoding($string,"UTF-8") ) {
                                print_error_and_exit(23, "Wrong string encoding!", $instruction_counter, $current_operand_postion);
                            }

                            # Scan the string for unexpected characters and escapes.
                            # The escapes need to be formated correctly: \[0-9][0-9][0-9] 
                            for ( $str_idx = 0; $str_idx < $string_lenght; $str_idx++ ) {
                                if ( strcmp("#", $string[$str_idx]) == 0 || (ord($string[$str_idx]) <= 32 && ord($string[$str_idx]) >= 0) ) {  
                                    print_error_and_exit(23, "Found unexpected character in a string!", $instruction_counter, $current_operand_postion);
                                }
                                if ( (strcmp($string[$str_idx], "\\") == 0) ) {
                                    if ( $str_idx >= $string_lenght-3 ) { # Handle the escape sequence at the end of string -> the elseif part wont segfault
                                        print_error_and_exit(23, "Found unfinished escape sequence or \\ in a string!", $instruction_counter, $current_operand_postion);
                                    }
                                    elseif ( !is_numeric($string[$str_idx+1]) || !is_numeric($string[$str_idx+2]) || !is_numeric($string[$str_idx+3]) ){
                                        print_error_and_exit(23, "Wrong escape sequence error in a string!", $instruction_counter, $current_operand_postion);
                                    }
                                }
                            }

                    }
                    # At this point we know that the operand is a constant so we
                    # can create XML argument and finish the validation of this argument
                    create_xml_arg($xmlw,$current_operand_postion,$current_operand_parts[0],$current_operand_parts[1]);
                    break;
                }
                # At this point the operand wasn't matched as a constant, so
                # we need to check if it isn't a variable => we do not break;
            case OperandTypes::variable:
                $validity = preg_match($variable_pattern, $current_operand);
                if ( $validity === false ) {
                    print_error_and_exit(99, "Function preg_match failed!", $instruction_counter, $current_operand_postion);
                }
                elseif ( $validity == 0 ) {
                    print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                }
                create_xml_arg($xmlw,$current_operand_postion, "var", $current_operand);
                break; 
            case OperandTypes::label:
                $validity = preg_match($label_pattern, $current_operand);
                if ( $validity === false ) {
                    print_error_and_exit(99, "Function preg_match failed!", $instruction_counter, $current_operand_postion);
                }
                elseif ( $validity == 0 ) {
                    print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                }
                create_xml_arg($xmlw,$current_operand_postion, "label", $current_operand);
                break;
            case OperandTypes::type:
                $validity = preg_match($type_pattern, $current_operand);
                if ( $validity === false ) {
                    print_error_and_exit(99, "Function preg_match failed!", $instruction_counter, $current_operand_postion);
                }
                elseif ( $validity == 0 ) {
                    print_error_and_exit(23, "Incorrect operand!", $instruction_counter, $current_operand_postion);
                }
                create_xml_arg($xmlw,$current_operand_postion, "type", $current_operand);
                break;
        }
    }
    $xmlw->endElement(); #end instruction element
}

$xmlw->endElement(); #end program element
$xmlw->endDocument();
echo $xmlw->outputMemory();
exit(0);
?>
